﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MovieTitles.Models;

namespace MovieTitles.Controllers
{   
    public class TitlesController : Controller
    {
        private TitlesContext db = new TitlesContext();


        /// <summary>
        /// SearchTitles
        /// </summary>
        /// <param name="movieGenre"></param>
        /// <param name="searchString"></param>
        /// <returns>Action Results</returns>
        public ActionResult Index(string movieGenre, string searchString)
        {
            // Keep a seperate list of Genres
            var GenreLst = new List<string>();

            var GenreQry = from g in db.Genres
                           orderby g.Name
                           select g.Name;


            GenreLst.AddRange(GenreQry.Distinct());
            ViewData["movieGenre"] = new SelectList(GenreLst);

            var titles = (from t in db.Titles
                          orderby t.TitleName
                          select t).Distinct();


            // Search for our titles based on a input string
            if (!String.IsNullOrEmpty(searchString))
            {
                titles = titles.Where(ta => ta.TitleName.Contains(searchString));
            }

            // If Genres are not being search return the default list of titles
            if (string.IsNullOrEmpty(movieGenre))
            {
                if (!String.IsNullOrEmpty(searchString))
                {
                    return View(titles);
                }

                else
                {   //                 
                    return View(titles);
                }

            }
            else
            {   // Get the list by Genres if necessary
                var genres = (from t in db.Titles
                              join tg in db.TitleGenres on t.TitleId equals tg.TitleId
                              join gr in db.Genres on tg.GenreId equals gr.Id
                              where gr.Name == movieGenre
                              orderby t.TitleName
                              select t).Distinct();


                return View(genres);
            }

        }

        /// <summary>
        /// Details
        /// </summary>
        /// <param name="id"></param>
        /// <returns>ActionResult</returns>
        public ActionResult Details(int id = 0)
        {
            Title title = db.Titles.Single(t => t.TitleId == id);
            if (title == null)
            {
                return HttpNotFound();
            }
            return View(title);
        }
        
        /// <summary>
        /// Dispose of all unnecessary resources by Default
        /// </summary>
        /// <param name="disposing"></param>
        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}