﻿using MovieTitles.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
using System.Data.EntityClient;
using System.Data.Objects;

namespace MovieTitles.Tests
{
    
    
    /// <summary>
    ///This is a test class for TitlesContextTest and is intended
    ///to contain all TitlesContextTest Unit Tests
    ///</summary>
    [TestClass()]
    public class TitlesContextTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for TitlesContext Constructor
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("C:\\Dev\\MovieTitles\\MovieTitles", "/")]
        [UrlToTest("http://localhost:50152/")]
        public void TitlesContextConstructorTest()
        {
            EntityConnection connection = null; // TODO: Initialize to an appropriate value
            TitlesContext target = new TitlesContext(connection);
            Assert.Inconclusive("TODO: Implement code to verify target");
        }

        /// <summary>
        ///A test for TitlesContext Constructor
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("C:\\Dev\\MovieTitles\\MovieTitles", "/")]
        [UrlToTest("http://localhost:50152/")]
        public void TitlesContextConstructorTest1()
        {
            string connectionString = string.Empty; // TODO: Initialize to an appropriate value
            TitlesContext target = new TitlesContext(connectionString);
            Assert.Inconclusive("TODO: Implement code to verify target");
        }

        /// <summary>
        ///A test for TitlesContext Constructor
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("C:\\Dev\\MovieTitles\\MovieTitles", "/")]
        [UrlToTest("http://localhost:50152/")]
        public void TitlesContextConstructorTest2()
        {
            TitlesContext target = new TitlesContext();
            Assert.Inconclusive("TODO: Implement code to verify target");
        }

        /// <summary>
        ///A test for AddToAwards
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("C:\\Dev\\MovieTitles\\MovieTitles", "/")]
        [UrlToTest("http://localhost:50152/")]
        public void AddToAwardsTest()
        {
            TitlesContext target = new TitlesContext(); // TODO: Initialize to an appropriate value
            Award award = null; // TODO: Initialize to an appropriate value
            target.AddToAwards(award);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for AddToGenres
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("C:\\Dev\\MovieTitles\\MovieTitles", "/")]
        [UrlToTest("http://localhost:50152/")]
        public void AddToGenresTest()
        {
            TitlesContext target = new TitlesContext(); // TODO: Initialize to an appropriate value
            Genre genre = null; // TODO: Initialize to an appropriate value
            target.AddToGenres(genre);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for AddToOtherNames
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("C:\\Dev\\MovieTitles\\MovieTitles", "/")]
        [UrlToTest("http://localhost:50152/")]
        public void AddToOtherNamesTest()
        {
            TitlesContext target = new TitlesContext(); // TODO: Initialize to an appropriate value
            OtherName otherName = null; // TODO: Initialize to an appropriate value
            target.AddToOtherNames(otherName);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for AddToParticipants
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("C:\\Dev\\MovieTitles\\MovieTitles", "/")]
        [UrlToTest("http://localhost:50152/")]
        public void AddToParticipantsTest()
        {
            TitlesContext target = new TitlesContext(); // TODO: Initialize to an appropriate value
            Participant participant = null; // TODO: Initialize to an appropriate value
            target.AddToParticipants(participant);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for AddToStoryLines
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("C:\\Dev\\MovieTitles\\MovieTitles", "/")]
        [UrlToTest("http://localhost:50152/")]
        public void AddToStoryLinesTest()
        {
            TitlesContext target = new TitlesContext(); // TODO: Initialize to an appropriate value
            StoryLine storyLine = null; // TODO: Initialize to an appropriate value
            target.AddToStoryLines(storyLine);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for AddToTitleGenres
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("C:\\Dev\\MovieTitles\\MovieTitles", "/")]
        [UrlToTest("http://localhost:50152/")]
        public void AddToTitleGenresTest()
        {
            TitlesContext target = new TitlesContext(); // TODO: Initialize to an appropriate value
            TitleGenre titleGenre = null; // TODO: Initialize to an appropriate value
            target.AddToTitleGenres(titleGenre);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for AddToTitleParticipants
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("C:\\Dev\\MovieTitles\\MovieTitles", "/")]
        [UrlToTest("http://localhost:50152/")]
        public void AddToTitleParticipantsTest()
        {
            TitlesContext target = new TitlesContext(); // TODO: Initialize to an appropriate value
            TitleParticipant titleParticipant = null; // TODO: Initialize to an appropriate value
            target.AddToTitleParticipants(titleParticipant);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for AddToTitles
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("C:\\Dev\\MovieTitles\\MovieTitles", "/")]
        [UrlToTest("http://localhost:50152/")]
        public void AddToTitlesTest()
        {
            TitlesContext target = new TitlesContext(); // TODO: Initialize to an appropriate value
            Title title = null; // TODO: Initialize to an appropriate value
            target.AddToTitles(title);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for Awards
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("C:\\Dev\\MovieTitles\\MovieTitles", "/")]
        [UrlToTest("http://localhost:50152/")]
        public void AwardsTest()
        {
            TitlesContext target = new TitlesContext(); // TODO: Initialize to an appropriate value
            ObjectSet<Award> actual;
            actual = target.Awards;
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Genres
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("C:\\Dev\\MovieTitles\\MovieTitles", "/")]
        [UrlToTest("http://localhost:50152/")]
        public void GenresTest()
        {
            TitlesContext target = new TitlesContext(); // TODO: Initialize to an appropriate value
            ObjectSet<Genre> actual;
            actual = target.Genres;
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for OtherNames
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("C:\\Dev\\MovieTitles\\MovieTitles", "/")]
        [UrlToTest("http://localhost:50152/")]
        public void OtherNamesTest()
        {
            TitlesContext target = new TitlesContext(); // TODO: Initialize to an appropriate value
            ObjectSet<OtherName> actual;
            actual = target.OtherNames;
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Participants
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("C:\\Dev\\MovieTitles\\MovieTitles", "/")]
        [UrlToTest("http://localhost:50152/")]
        public void ParticipantsTest()
        {
            TitlesContext target = new TitlesContext(); // TODO: Initialize to an appropriate value
            ObjectSet<Participant> actual;
            actual = target.Participants;
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for StoryLines
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("C:\\Dev\\MovieTitles\\MovieTitles", "/")]
        [UrlToTest("http://localhost:50152/")]
        public void StoryLinesTest()
        {
            TitlesContext target = new TitlesContext(); // TODO: Initialize to an appropriate value
            ObjectSet<StoryLine> actual;
            actual = target.StoryLines;
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for TitleGenres
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("C:\\Dev\\MovieTitles\\MovieTitles", "/")]
        [UrlToTest("http://localhost:50152/")]
        public void TitleGenresTest()
        {
            TitlesContext target = new TitlesContext(); // TODO: Initialize to an appropriate value
            ObjectSet<TitleGenre> actual;
            actual = target.TitleGenres;
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for TitleParticipants
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("C:\\Dev\\MovieTitles\\MovieTitles", "/")]
        [UrlToTest("http://localhost:50152/")]
        public void TitleParticipantsTest()
        {
            TitlesContext target = new TitlesContext(); // TODO: Initialize to an appropriate value
            ObjectSet<TitleParticipant> actual;
            actual = target.TitleParticipants;
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Titles
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("C:\\Dev\\MovieTitles\\MovieTitles", "/")]
        [UrlToTest("http://localhost:50152/")]
        public void TitlesTest()
        {
            TitlesContext target = new TitlesContext(); // TODO: Initialize to an appropriate value
            ObjectSet<Title> actual;
            actual = target.Titles;
            Assert.Inconclusive("Verify the correctness of this test method.");
        }
    }
}
